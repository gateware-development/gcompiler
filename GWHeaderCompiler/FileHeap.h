#ifndef _RESOURCEHEAP_H_
#define _RESOURCEHEAP_H_
#include <unordered_map>
#include "File.h"

// Heap in terms of memory, not data structure
class FileHeap
{
private:
	std::unordered_map<std::string, File*> files;
	GW::SYSTEM::GFile fileHandler;

public:
	FileHeap();
	~FileHeap();

	// Returns nullptr if file already exists in our current heap
	File* Create(const std::string& _path, const std::string& _name);
	// Returns nullptr if file does not exist
	File* Find(const std::string& _name) const;

	std::unordered_map<std::string, File*>::iterator begin() { return files.begin(); }
	std::unordered_map<std::string, File*>::iterator end() { return files.end(); }
	std::unordered_map<std::string, File*>::const_iterator cbegin() const { return files.cbegin(); }
	std::unordered_map<std::string, File*>::const_iterator cend() const { return files.cend(); }
};
#endif