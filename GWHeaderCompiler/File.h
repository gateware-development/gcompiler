#ifndef _FILE_H_
#define _FILE_H_
#include <vector>
#include "pch.h"
#include <string>

class FileHeap;

class File
{
private:
	enum class FileType
	{
		eH,
		eHPP,
		eOther
	} type;

	//unsigned int dependencyCount = 0u; // how many files depend on this file


	std::string path; // absolute path
	std::string name;
	std::string fullPath; // path + name
	std::string rawData; // raw data of the file
	std::vector<File*> includeFiles;
	GW::SYSTEM::GFile* fileHandler = nullptr;



	// get the file exention with out the .
	std::string GetFileExtension(const std::string& name) const;
	void DetermineFileType();

	void WarnThatWeAreAnExternalFile();
	void ReadDataFromFile(unsigned int& fileSize);
	std::string RetrieveFileName(const std::string _fileName);
	void AddFileToHeapAndVector(const std::string _path, const std::string _name, FileHeap&_heap, std::vector<File*>& _fileVector);

	std::string GetAbsolutePath(std::string _relativePath);
	bool isExternalFile;


public:
	File(const std::string& _path, const std::string& _name, GW::SYSTEM::GFile* _fileHandler);
	~File() = default;

	//unsigned int GetDependencyCount() const;

	const std::string& GetPath() const;
	const std::string& GetName() const;
	const std::string& GetFullPath() const;
	unsigned int GetByteSize() const;
	const std::string& GetRawData() const;
	const std::vector<File*>& GetIncludeFiles() const;

	void Preprocess(FileHeap& headerHeap, FileHeap& sourceHeap);

	bool IsH() const;
	bool IsHPP() const;
	bool IsExternalFile() const;
};
#endif