#include "pch.h"
#include "Directory.h"
#include "GWException.h"
#include "PredefinedRegex.h"
#define MAX_CHARACTER_LENGTH 260

Directory::Directory(const std::string& _path, const std::string& _name, unsigned int _depth)
	: path(_path), name(_name), depth(_depth), isRoot(false)
{
	// ignore unit test folder
	if (name == "UnitTests")
		return;

		ThrowIfFailed(fileHandler.Create());

	isRoot = (depth == 0);

	fullPath = path + FILE_DELIMITER + name;

	GW::SYSTEM::GFile gFile;
	ThrowIfFailed(gFile.Create());
	ThrowIfFailed(gFile.SetCurrentWorkingDirectory(fullPath.data()));
	if (isRoot)
	{
		ThrowIfFailed(gFile.SetCurrentWorkingDirectory(fullPath.data()));
		rootFullPath.resize(MAX_CHARACTER_LENGTH);
		gFile.GetCurrentWorkingDirectory(const_cast<char*>(rootFullPath.data()), MAX_CHARACTER_LENGTH);
		path.clear();


		path = rootFullPath.substr(0, rootFullPath.find_last_of(FILE_DELIMITER));
		path = path.substr(0, path.find_last_of(FILE_DELIMITER));
		fullPath = path + FILE_DELIMITER + name;
	}

	unsigned int fileCount = 0;
	ThrowIfFailed(gFile.GetDirectorySize(fileCount));
	unsigned int folderCount = 0;
	ThrowIfFailed(gFile.GetSubDirectorySize(folderCount));

	unsigned int i;
	char** charPPFiles = new char* [fileCount];
	for (i = 0; i < fileCount; ++i)
		charPPFiles[i] = new char[MAX_CHARACTER_LENGTH];
	char** charPPSubDir = new char* [folderCount];
	for (i = 0; i < folderCount; ++i)
		charPPSubDir[i] = new char[MAX_CHARACTER_LENGTH];

	// read files from dir
	ThrowIfFailed(gFile.GetFilesFromDirectory(charPPFiles, fileCount, MAX_CHARACTER_LENGTH));
	ThrowIfFailed(gFile.GetFoldersFromDirectory(folderCount, MAX_CHARACTER_LENGTH, charPPSubDir));

	// give me all the files in the filePath
	// populate all files vector, NOM NOM NOM NOM
	for (i = 0; i < fileCount; ++i)
	{
		std::string l_fileName = charPPFiles[i];
		if (l_fileName.substr(l_fileName.find_last_of(".") + 1) == "h" ||
			l_fileName.substr(l_fileName.find_last_of(".") + 1) == "hpp")
			files.push_back(new File(fullPath, charPPFiles[i], &fileHandler));
	}
	for (i = 0; i < folderCount; ++i)
		subdirectories.push_back(new Directory(fullPath, charPPSubDir[i], depth + 1));

	// clean up memory
	for (i = 0; i < fileCount; ++i)
		delete[] charPPFiles[i];
	delete[] charPPFiles;
	for (i = 0; i < folderCount; ++i)
		delete[] charPPSubDir[i];
	delete[] charPPSubDir;
}

Directory::~Directory()
{
	size_t i;
	for (i = 0; i < files.size(); ++i)
		delete files[i];
	files.clear();

	for (i = 0; i < subdirectories.size(); ++i)
		delete subdirectories[i];
	subdirectories.clear();
}

unsigned int Directory::GetDepth() const { return depth; }
const std::string& Directory::GetPath() const { return path; }
const std::string& Directory::GetName() const { return name; }
const std::string& Directory::GetFullPath() const { return fullPath; }
const std::vector<File*>& Directory::GetFiles() const { return files; }
const std::vector<Directory*>& Directory::GetSubdirectories() const { return subdirectories; }