#ifndef _COMMAND_LINE_ARGUMENT_HEADER
#define _COMMAND_LINE_ARGUMENT_HEADER


#include <vector>
#include <string>
#include <iostream>

class CommandLineArgumentHandler
{
private:
	std::string interfacePath, outputPath, licensePath, headerListPath;
	std::vector<std::string> commandLineArguments, prependFilePaths, appendFilePaths;


	void CopyArgumentsIntoVector(int _argc, char** _argv);

	void FindInterfacePath();
	void FindOutputPath();
	void FindLicensePath();
	void FindPrependFilePaths();
	void FindAppendFilePaths();
	void FindHeaderListPath();

	int IndexOfArgument(std::string _argumentToFind);


public:
	CommandLineArgumentHandler(int _argc, char** _argv);


	bool ArgumentsAreValid();
	bool HelpWasRequested();
	bool ShouldGenerate();

	std::string GetOutputPath();
	std::string GetInterfacePath();
	std::string GetLicensePath();
	std::string GetHeaderListPath();

	std::vector<std::string> GetPrependFilePaths();
	std::vector<std::string> GetAppendFilePaths();

	void PrintHelpInfo();
};


#endif