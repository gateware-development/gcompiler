#ifndef _FILE_SORTER
#define _FILE_SORTER
#include "pch.h"
#include "FileHeap.h"

class FileSorter
{
private:
	bool headersAreSorted = false;
	FileHeap* headerHeap = nullptr;
	std::vector<File*> headerFiles;
	std::string headerListPath;

	void CopyFileHeapToVector(FileHeap* _from, std::vector<File*>& _to);
	int GetFileIndexByName(std::string _fileName, std::vector<File*> _fileList);

public:
	FileSorter() = default;
	FileSorter(FileHeap* _headerHeap);
	
	std::vector<File*>* GetSortedHeaderList();
	void SortHeaders();
	void SetHeaderListPath(std::string _headerListPath);

private:
	std::vector<std::string> desiredHeaderOrder;
	void ReadHeaderListFromFile();
};
#endif