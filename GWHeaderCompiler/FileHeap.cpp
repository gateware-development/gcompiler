#include "FileHeap.h"
#include "GWException.h"
FileHeap::FileHeap()
{
	ThrowIfFailed(fileHandler.Create());
}

FileHeap::~FileHeap()
{
	for (auto iter = begin(); iter != end(); ++iter)
	{
		delete iter->second;
		iter->second = nullptr;
	}
	files.clear();
}

File* FileHeap::Create(const std::string& _path, const std::string& _name)
{
	auto iter = files.find(_name);
	if (iter == end())
	{
		files[_name] = new File(_path, _name, &fileHandler);
		return files[_name];
	}
	else
		return nullptr;
}

File* FileHeap::Find(const std::string& _name) const
{
	auto iter = files.find(_name);
	if (iter == cend())
		return nullptr;
	else
		return iter->second;
}