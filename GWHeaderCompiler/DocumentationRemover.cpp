#include "DocumentationRemover.h"

DocumentationRemover::DocumentationRemover(std::string _outputPath)
{
	ThrowIfFailed(fileHandler.Create());
	ThrowIfFailed(fileHandler.SetCurrentWorkingDirectory(_outputPath.data()));
}

void DocumentationRemover::OpenWrite()
{
	if (readIsOpen)
		CloseFile();

	ThrowIfFailed(fileHandler.OpenTextWrite("Gateware.h"));
	writeIsOpen = true;
}
void DocumentationRemover::OpenRead()
{
	if (writeIsOpen)
		CloseFile();

	ThrowIfFailed(fileHandler.OpenTextRead("Gateware.h"));
	readIsOpen = true;
}
void DocumentationRemover::CloseFile()
{
	fileHandler.CloseFile();
	writeIsOpen = false;
	readIsOpen = false;
}

void DocumentationRemover::RemoveDoxygen()
{
	char cLine[1024];
	std::string line;
	line.resize(1024);
	unsigned int currPos;
	std::string cleanGWHeader;


	auto clearLines = [&]() {
		line.clear();
		memset(cLine, 0, sizeof(cLine));
	};

	auto readLine = [&]() {
		clearLines();
		fileHandler.ReadLine(cLine, 1024, '\n');
		line = cLine;
	};

	auto skipLine = [&]() {
		// intentionally left blank.
	};

	auto findInLine = [&](const char* c) {
		return line.find(c) != std::string::npos ? true : false;
	};

	auto getIndexOf = [&](const char* c) {
		return line.find(c); // assumes that "c" is in line
	};

	auto skipLinesUntil = [&](const char* c) {
		// first check if its in the current line already
		if (findInLine(c)) {
			skipLine();
			return;
		}
		else { // loop until we find it
			while (+fileHandler.Seek(-1, 0, currPos)) {
				readLine();
				if (findInLine(c)) {
					skipLine();
					break;
				}
			}
		}
	};

	auto endOfFile = [&]() {
		return -fileHandler.Seek(-1, 0, currPos);
	};

	OpenRead();

	while (!endOfFile()) {
		readLine();

		if (findInLine("#ifdef DOXYGEN_ONLY")) {
			skipLinesUntil("#endif");
		}
		else if (findInLine("/*!") && !findInLine("/*!<")) {
			skipLinesUntil("*/");
		}
		else if (findInLine("//!") && !findInLine("//!<")) {
			skipLine();
		}
		else if (findInLine("/*!<")) {
			auto i = getIndexOf("/*!<");
			line[i] = '\0';
			cleanGWHeader.append(line.c_str());
			cleanGWHeader.append("\n");
		}
		else if (findInLine("//!<")) {
			auto i = getIndexOf("//!<");
			line[i] = '\0';
			cleanGWHeader.append(line.c_str());
			cleanGWHeader.append("\n");
		}
		else {
			cleanGWHeader.append(line.c_str());
			cleanGWHeader.append("\n");
		}
	}

	// reopen Gateware.h to truncate it
	CloseFile();
	OpenWrite();

	// write out the clean Gateware.h
	fileHandler.WriteLine(cleanGWHeader.c_str());
	fileHandler.FlushFile();
	CloseFile();
}