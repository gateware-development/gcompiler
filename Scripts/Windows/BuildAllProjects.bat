@echo on

if not exist "../Gateware-Development"  call "ShallowCloneDevelopmentRepo.bat" 

cd ../../
cd Gateware-Development
if not exist "build" mkdir build

cd build
if not exist "VisualStudio" mkdir VisualStudio

cd VisualStudio
cmake -A x64 "../../../"