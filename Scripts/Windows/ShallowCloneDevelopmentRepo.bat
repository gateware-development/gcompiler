@echo off

setlocal EnableDelayedExpansion
SETLOCAL

::switches
set /a Overwrite=1
set /a PrintHelp=0

::loop through arguments and set switches accordingly
for %%x in (%*) do (
    if /i "%%x" == "--NoOverwrite" set /a Overwrite=0
    if /i "%%x" == "--Help" set /a PrintHelp=1
)


if !PrintHelp! == 1 (
    echo This script automatically clones GSource into a folder named Gateware-Development
    echo Arguments:
    echo   --Help           
    echo        Prints this help text
    echo   --NoOverwrite    
    echo        This script will not delete an existing clone if one exists
    EXIT /B
)

::Remove existing repo if asked to
if !Overwrite! == 1 (
    cd ../../ 
    if exist Gateware-Development RMDIR Gateware-Development /q /s 
    cd Scripts/Windows 
)

 if not exist ../../Gateware-Development (
    git clone https://gitlab.com/gateware-development/gsource.git ../../Gateware-Development --depth=1 --branch master --single-branch
 )

ENDLOCAL