#this script will not be able to run unless CompileSingleHeader has already been run!
cd ../../

cmake --build "./Gateware-Development/build/VisualStudio" --target "SingleHeaderTestMain" --config "Debug"
cmake --build "./Gateware-Development/build/VisualStudio" --target "SingleHeaderTestMain" --config "Release"

cd Gateware-Development/build/VisualStudio/SingleHeaderTest

echo "Running tests in debug mode"
./Debug/SingleHeaderTestMain.exe

echo "Running tests in release mode"
./Release/SingleHeaderTestMain.exe

pause