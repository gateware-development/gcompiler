cd ../../

#remove a cloned repo if one already exists so we always compile from a fresh clone
if(Test-Path Gateware-Development){Remove-Item Gateware-Development -Recurse -Force}

#shallow clone the source repo
git clone https://gitlab.com/gateware-development/gsource.git Gateware-Development --depth=1 --branch master --single-branch
#Set up the needed directories
cd Gateware-Development
if(-not(Test-Path build)) {mkdir build}
cd build
if(-not(Test-Path Desktop)) {mkdir Desktop}
cd Desktop
cmake -A x64 "../../../"  #make all the projects 
cmake --build "./" --target "SingleHeaderCompiler" --config "Release" #build the SingleHeaderCompiler executable 
mv GWHeaderCompiler/Release/SingleHeaderCompiler.exe ../ -force #move the executable to root build folder 

#generate an up-to-date version hpp and txt 
cd ../../../
cd Gateware-Development/DevOps
cmake -P ./GenVerHpp.cmake 

#Compile a single header 
cd ../../
cd Scripts
cmake -P ./GenSingleHeader.cmake

#return to the starting directory (ending a script in a different directory than it starts in is bad manners)
cd Windows