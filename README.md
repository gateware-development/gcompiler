# GCompiler

This repo is designed to house any and all functionality related to automating Gateware's release process. 
The two primary tasks this repo is meant to do are the single-header compilation and doxygen generation, but other automatable tasks may also be included.
